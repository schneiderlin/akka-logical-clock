package raft

package object protocol extends Serializable
  with InternalProtocol
  with NodeState
