package raft.protocol

import akka.actor.ActorRef
import raft.ClusterConfiguration
import raft.model.{Entry, Term}

object RaftProtocol {

  case class RequestVote(term: Term,
                         candidateId: ActorRef,
                         lastLogTerm: Term,
                         lastLogIndex: Int)

  case class AppendEntries[T](term: Term,
                              prevLogTerm: Term,
                              prevLogIndex: Int,
                              entries: Seq[Entry[T]],
                              leaderCommit: Int)

  case class ChangeConfiguration(newConf: ClusterConfiguration)
}
