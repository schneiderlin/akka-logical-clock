package raft.protocol

import raft.model.Term

trait InternalProtocol {

  // 需要更多的entries
  case class AppendRejected(term: Term) extends InternalProtocol

  // 收到来自以前term的消息
  @Deprecated
  case class YourAreOutDated(term: Term) extends InternalProtocol

  case object ElectionTimeout extends InternalProtocol

}
