package raft.protocol

trait NodeState {

  case object Follower extends NodeState

  case object Candidate extends NodeState

  case object Leader extends NodeState

}