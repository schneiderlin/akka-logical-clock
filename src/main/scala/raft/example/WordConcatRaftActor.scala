package raft.example

import akka.actor.Props
import raft.NodeActor
import raft.example.protocol._

class WordConcatRaftActor extends NodeActor {
  override type Command = Cmd

  var words: Vector[String] = Vector()

  override def apply: ReplicateStateMachineApply = {
    case AppendWord(word) =>
      words = words :+ word
      log.info(s"Applied command [AppendWord($word)], full words is: $words")

      word

    case GetWords =>
      log.info("Replying with {}", words.toList)
      words.toList
  }
}

object WordConcatRaftActor {
  def props: Props = Props(new WordConcatRaftActor())
}