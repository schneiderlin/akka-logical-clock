package raft.example.protocol

trait WordConcatProtocol {

  sealed trait Cmd

  case class AppendWord(word: String) extends Cmd

  case object GetWords extends Cmd

}
