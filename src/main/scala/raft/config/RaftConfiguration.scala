package raft.config

import akka.actor.{ExtendedActorSystem, Extension, ExtensionId, ExtensionIdProvider}

/**
  * Extension是一个class，ExtensionId是一个具体的object
  */
object RaftConfiguration extends ExtensionId[RaftConfig] with ExtensionIdProvider {

  /**
    * 如何创建这个Extension instance
    */
  override def createExtension(system: ExtendedActorSystem): RaftConfig = new RaftConfig(system.settings.config)

  /**
    * 用ExtensionIdProvider进行lookup，好像是实现的细节，为了把Extension绑到ActorSystem里面
    */
  override def lookup(): ExtensionId[_ <: Extension] = RaftConfiguration
}
