package raft

trait ReplicateStateMachine {
  type ReplicateStateMachineApply = PartialFunction[Any, Any]

  def apply: ReplicateStateMachineApply
}
