package raft.model

import akka.actor.ActorRef

case class Entry[T](command: T,
                    term: Term,
                    index: Int,
                    client: Option[ActorRef] = None)