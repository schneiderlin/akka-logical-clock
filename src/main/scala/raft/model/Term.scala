package raft.model

case class Term(termNr: Long) extends AnyVal {
  def prev: Term = this - 1

  def -(n: Long): Term = Term(termNr - n)

  def next: Term = this + 1

  def +(n: Long): Term = Term(termNr + n)

  def >(otherTerm: Term): Boolean = this.termNr > otherTerm.termNr

  def <(otherTerm: Term): Boolean = this.termNr < otherTerm.termNr

  def >=(otherTerm: Term): Boolean = this.termNr >= otherTerm.termNr

  def <=(otherTerm: Term): Boolean = this.termNr <= otherTerm.termNr
}

object Term {
  val Zero = Term(0)
}