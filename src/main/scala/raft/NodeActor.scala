package raft

import akka.actor.FSM
import akka.dispatch.forkjoin.ThreadLocalRandom
import raft.config.RaftConfiguration
import raft.model.Term
import raft.protocol._
import raft.protocol.RaftProtocol.{AppendEntries, RequestVote}

import scala.concurrent.duration._

abstract class NodeActor
  extends FSM[NodeState, NodeData]
    with ReplicateStateMachine with SharedBehaviors {

  type Command

  protected val raftConfig = RaftConfiguration(context.system)

  private val ElectionTimeoutTimerName = "election-timer"

  var electionDeadline: Deadline = 0 seconds fromNow

  // raft member state ------------------------------------------

  var replicatedLog: ReplicatedLog[Command] = ReplicatedLog.empty

  //  var nextIndex

  // end of raft member state -----------------------------------

  startWith(Follower, NodeData.initial)

  when(Follower, stateTimeout = raftConfig.electionTimeoutMax) {
    clusterManagementBehavior orElse {
      case Event(
      RequestVote(term, candidateId, lastLogTerm, lastLogIndex),
      data: NodeData) => ???

      case Event(StateTimeout, data: NodeData) =>
        goto(Candidate) using data
    }
  }

  when(Candidate, stateTimeout = raftConfig.electionTimeoutMax) {
    clusterManagementBehavior orElse {
      case Event(
      RequestVote(term, candidateId, lastLogTerm, lastLogIndex),
      data: NodeData) => ???
      case Event(StateTimeout, data: NodeData) =>
        goto(Candidate) using data
    }
  }

  // TODO: configurable heard beat timeout
  when(Leader, stateTimeout = 1 second) {
    clusterManagementBehavior orElse {
      case Event(
      RequestVote(term, candidateId, lastLogTerm, lastLogIndex),
      data: NodeData) => ???
    }
  }

  whenUnhandled {
    clusterManagementBehavior orElse {
      case Event(
      AppendEntries(term, prevLogTerm, prevLogIndex, entries, leaderCommit),
      data: NodeData) => {
        compareTerm(term, data)

        if (prevLogIndex == -1) { // 我的logs和leader的logs全部冲突（我的logs是空），全部替换
          if (leaderCommit > replicatedLog.committedIndex) {
            if (replicatedLog.getLastLogIndex < leaderCommit) {
              // TODO: commit all
            } else {
              // TODO: commit keep up to leader
            }
          }
          // TODO: replace all entries and update commit
          stay using ???
        } else if (replicatedLog.entries.length > prevLogIndex && replicatedLog.entries(prevLogIndex).term != prevLogTerm) {
          // TODO: this logic seems wrong, double check the paper later
          // give me more entries
          sender() ! AppendRejected(data.currentTerm)
        } else {
          // TODO: update entries
        }
        ???
      }

      case Event(
      RequestVote(term, candidateId, lastLogTerm, lastLogIndex),
      data: NodeData) => {

        ???
      }

      case _ =>
        log.warning("received unhandled message")
        stay
    }
  }

  onTransition {
    case Follower -> Candidate =>
      startElection()
    case Candidate -> Candidate =>
      startElection()
  }

  initialize()

  def beginElection(data: NodeData) = {
    resetElectionDeadline()

    goto(Candidate)
  }

  def resetElectionDeadline(): Deadline = {
    cancelTimer(ElectionTimeoutTimerName)

    electionDeadline = nextElectionDeadline()
    log.debug("Resetting election timeout: {}", electionDeadline)
    setTimer(ElectionTimeoutTimerName, ElectionTimeout, electionDeadline.timeLeft, repeat = false)
    electionDeadline
  }

  def nextElectionDeadline(): Deadline = randomElectionTimeout(
    from = raftConfig.electionTimeoutMin,
    to = raftConfig.electionTimeoutMax
  ).fromNow

  private def randomElectionTimeout(from: FiniteDuration, to: FiniteDuration): FiniteDuration = {
    val fromMs = from.toMillis
    val toMs = to.toMillis
    require(toMs > fromMs)

    (fromMs + ThreadLocalRandom.current().nextInt(toMs.toInt - fromMs.toInt)).millis
  }

  // helpers --------------------------------------------------------------------------------------------------

  def acceptHeartbeat(): State = {
    resetElectionDeadline()
    stay()
  }

  def cancelElectionDeadline(): Unit = {
    cancelTimer(ElectionTimeoutTimerName)
  }

  @Deprecated
  private def startElection(): Unit = {
    val newTerm = stateData.currentTerm + 1
    val newElectionData = ElectionData(voteCount = 1, voteFor = Some(stateData.clusterSelf))
    stateData.config.members foreach {
      peer => peer ! RequestVote(newTerm, self, replicatedLog.getLastLogTerm, replicatedLog.getLastLogIndex)
    }
  }

  // TODO: goto 或者 stay之后，会不会继续执行后面的代码
  // 注释里面说Return this from a state function in order to effect the transition.
  // 似乎goto必须放在最后
  @Deprecated
  private def compareTerm(receivedTerm: Term, data: NodeData) = {
    if (receivedTerm > data.currentTerm) {
      goto(Follower) using data.copy(currentTerm = receivedTerm)
    } else if (receivedTerm < data.currentTerm) {
      sender() ! YourAreOutDated(data.currentTerm)
      stay
    }
  }
}

//object NodeActor {
//  def props() = Props(new NodeActor[Int](1 second, List(), 1))
//}