package raft

import akka.actor.ActorRef

sealed trait ClusterConfiguration {
  def members: Set[ActorRef]

}

case class StableClusterConfiguration(members: Set[ActorRef]) extends ClusterConfiguration {
}

object ClusterConfiguration {
  def apply(members: Iterable[ActorRef]): ClusterConfiguration =
    StableClusterConfiguration(members.toSet)

  def apply(members: ActorRef*): ClusterConfiguration =
    StableClusterConfiguration(members.toSet)
}