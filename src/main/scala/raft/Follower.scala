package raft


import raft.protocol._
import raft.protocol.RaftProtocol.AppendEntries

private[raft] trait Follower {
  this: NodeActor =>
  val followerBehavior: StateFunction = {
    case Event(msg: AppendEntries[Command], data: NodeData) =>
      acceptHeartbeat()

    case Event(ElectionTimeout, data: NodeData) =>
      // 有可能在收到ElectionTimeout的时候，deadline又被刷新了，避免不必要的选举
      if (electionDeadline.isOverdue()) ???
      else stay()
  }

  def leaderIsLagging(msg: AppendEntries[Command], data: NodeData): Boolean =
    msg.term < data.currentTerm
}
