package raft

import akka.actor.ActorRef
import raft.model.{Entry, Term}


case class ReplicatedLog[T](entries: List[Entry[T]],
                            committedIndex: Int) {
  def getLastLogIndex: Int = entries.length - 1

  def getLastLogTerm: Term = entries.last.term
}

object ReplicatedLog {
  def empty[T]: ReplicatedLog[T] = ReplicatedLog(List(), 0)
}

case class ElectionData(voteCount: Int, voteFor: Option[ActorRef])

object ElectionData {
  def initial: ElectionData = ElectionData(0, None)
}

case class NodeData(clusterSelf: ActorRef,
                    currentTerm: Term,
                    config: ClusterConfiguration,
                    electionData: ElectionData)

object NodeData {
  def initial(implicit self: ActorRef): NodeData =
    new NodeData(self, Term(0), ClusterConfiguration(), ElectionData.initial)
}