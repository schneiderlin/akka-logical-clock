package raft

import raft.protocol.RaftProtocol.ChangeConfiguration

trait SharedBehaviors {
  this: NodeActor =>

  private[raft] lazy val clusterManagementBehavior: StateFunction = {
    case Event(ChangeConfiguration(newConf), data: NodeData) =>
      // TODO: should use consensus algorithm to apply change
      stay using data.copy(config = newConf)

  }
}
