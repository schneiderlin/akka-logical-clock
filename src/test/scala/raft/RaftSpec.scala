package raft

import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest._
import org.scalatest.concurrent.Eventually
import raft.example.WordConcatRaftActor
import raft.protocol.RaftProtocol.ChangeConfiguration

abstract class RaftSpec(_system: Option[ActorSystem] = None) extends TestKit(_system getOrElse ActorSystem("raft-test"))
  with ImplicitSender with Eventually with FlatSpecLike with Matchers
  with BeforeAndAfterAll with BeforeAndAfterEach {

  var raftConfiguration: ClusterConfiguration = _
  protected var _members: Vector[ActorRef] = Vector.empty

  def initialMembers: Int

  override def beforeAll(): Unit = {
    super.beforeAll()
    (1 to initialMembers).foreach(i => createActor(s"raft-member-$i"))

    raftConfiguration = ClusterConfiguration(_members)
    _members.foreach(_ ! ChangeConfiguration(raftConfiguration))
  }

  def createActor(name: String): ActorRef = {
    val actor = system.actorOf(WordConcatRaftActor.props)
    _members :+= actor
    actor
  }
}
