package raft


import scala.concurrent.duration._

class InitialElectionSpec extends RaftSpec {

  behavior of "initial election"

  override val initialMembers = 3

  it should "select a leader within 3 seconds" in {
    within(3 seconds) {
      Thread.sleep(2500)
    }
  }
}