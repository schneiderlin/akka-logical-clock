import org.scalatest._
import org.scalatest.prop.PropertyChecks

class PropertyBaseBasicTest extends PropertyChecks with Matchers with FlatSpecLike {

  "fraction" should "" in {
    forAll { (n: Int, d: Int) =>
      whenever(d != 0 && d != Integer.MAX_VALUE && d != Integer.MIN_VALUE && n != Integer.MIN_VALUE) {
        val f = new Fraction(n, d)

        if (n < 0 && d < 0 || n > 0 && d > 0)
          f.numer should be > 0
        else if (n != 0)
          f.numer should be < 0
        else
          f.numer shouldBe 0

        f.denom should be > 0
      }
    }
  }
}

class Fraction(n: Int, d: Int) {

  require(d != 0)
  require(d != Integer.MIN_VALUE)
  require(n != Integer.MIN_VALUE)

  val numer = if (d < 0) -1 * n else n
  val denom = d.abs

  override def toString = numer + " / " + denom
}