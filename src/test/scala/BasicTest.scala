import akka.actor.{Actor, ActorRef, ActorSystem, PoisonPill, Props}
import akka.testkit.{ImplicitSender, TestActor, TestActors, TestKit, TestProbe}
import org.scalatest._

class BasicTest extends TestKit(ActorSystem("basic-test"))
  with ImplicitSender with FlatSpecLike with Matchers
  with BeforeAndAfterAll with BeforeAndAfterEach {

  behavior of "Basic test"

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  it should "echo" in {
    val echo = system.actorOf(TestActors.echoActorProps)
    echo ! "hello world"
    expectMsg("hello world")
  }

  it should "kill some actor with poison pill" in {
    val probe = TestProbe()
    val actor = system.actorOf(TestActors.echoActorProps)
    probe watch actor
    actor ! PoisonPill
    probe.expectTerminated(actor)
  }

  "a probe" should "able to forward message" in {
    // probe 站在了dest的前面，拦截到dest收到的信息，进行assert
    // assert完之后可以forward
    val probe = TestProbe()
    val source = system.actorOf(Props(new Source(probe.ref)))
    val dest = system.actorOf(Props(new Destination))

    source ! "start"
    probe.expectMsg("work")
    probe.forward(dest)
  }

  "a probe" should "auto pilot A --> Probe --> B" in {
    val probe = TestProbe()
    val source = system.actorOf(Props(new Source(probe.ref)))
    val dest = system.actorOf(Props(new Destination))

    probe.setAutoPilot((sender: ActorRef, msg: Any) => msg match {
      case "stop" => TestActor.NoAutoPilot
      case x =>
        testActor.tell(x, dest)
        TestActor.KeepRunning
    })

    source ! "start"
    probe.expectMsg("work")
  }
}

class Source(target: ActorRef) extends Actor {
  def receive = {
    case "start" => target ! "work"
  }
}

class Destination extends Actor {
  def receive = {
    case x => println(x)
  }
}